package com.wishwingz.example.empty.configurations;

import com.wishwingz.example.empty.controller.Controllers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = { Controllers.class })
public class ExampleWebApplicationContextConfig extends WebMvcConfigurerAdapter {

    /**
     * <pre>
     * InternalResourceViewResolver 는 서버에 있는 동적, 정적 리소스를 반환해 준다.
     * 1.Servlet 일 경우, 해당 서블릿 이름 or Path 로 식별하여, Servlet 을 실행해 준다.
     * 2.JSP 일 경우, .jsp, jspx 확장자로 식별하여 서블릿 컨테이너가 JspServlet 를 통해 처리하도록 해준다.
     * 3.JSP가 아닐 경우(html,jpg...), 기본설정만으로는 서블릿 컨테이너가 처리할 서블릿을 매핑하지 못한다. 이 경우에는 별도의 매핑설정이 추가되어야 한다.
     * (#ExpampleWebApplicationInitializer : JspServlet에 대한 매핑설정 추가 or DefaultServelt 에 대한 매핑설정 추가)
     * </pre>
     */
    @Bean
    public ViewResolver exampleViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setOrder(1);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;

    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

}
