package com.wishwingz.example.empty.configurations;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

public class ExampleWebApplicationInitializer implements WebApplicationInitializer{

    // 로딩해줄 스프링 설정
    private static final Class<?> WEB_APPLICATION_CONTEXT = ExampleWebApplicationContextConfig.class;

    @Override
    public void onStartup(ServletContext container) {
        AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        //Default 값은 true 다
        appContext.setAllowBeanDefinitionOverriding(true);
        appContext.register(WEB_APPLICATION_CONTEXT);
        ServletRegistration.Dynamic registration = container.addServlet("SpringWebMVC", new DispatcherServlet(appContext));

        registration.setLoadOnStartup(1);
        registration.addMapping("/");
    }
}
